<?php
/*
* version : 1.0.0
* Tripdetails_test()
* description :  This class is used for testing tripdetails and payment tab
* return : text with sucess or false;
* Author : Pardeep singh
*/

class Tripdetails_test extends TestCase
{
	/*
	* version : 1.0.0
	* test_index()
	* description :  This function is used for testing the getting index page for tripdetail page
	* return : text with sucess or false;
	* Author : Pardeep singh
	*/
	public function test_index()
	{
		global $argv, $argc;
		$uid	= isset($argv[5]) ? $argv[5] : 1;
		$post	= array('param'=>array('user' => $uid),'data' => '1');
		$output = $this->request("POST", "tripDetailsList" , $post);
		$this->assertContains('ok tested', $output);
	}
	
	
	/*
	* version : 1.0.0
	* test_sendEmail()
	* description :  This function is used for testing for getting email pop up html
	* return : text with sucess or false;
	* Author : Pardeep singh
	*/
	public function test_sendEmail(){
		global $argv, $argc;
		$toMail	= isset($argv[3]) ? $argv[3] : 'pardeep@test.com';
		$rid	= isset($argv[4]) ? $argv[4] : 1000;
		$post	= array('type' => $rid,'like' => 'json');
		$output = $this->request("POST", "sendEmailModal" , $post);
		$this->assertContains('<td> Trip Reference : ', $output);
	}
	
}