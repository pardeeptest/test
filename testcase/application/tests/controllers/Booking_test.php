<?php

class Booking_test extends TestCase
{
	
	/*
	* version : 1.0.0
	* test_step1()
	* description :  This function is used for checking First step of Booking portal.
	* @param : 
	* return true false text;
	* Author : Pardeep singh
	*/
	public function test_index(){
		$output = $this->request("GET", "booking/step1/Mg==");
		$this->assertContains("Step1 Code Ok.", $output);
	}

	/*
	* version : 1.0.0
	* test_step2()
	* description :  This function is used for checking Second step of Booking portal.
	* @param : 
	* return true false text;
	* Author : Pardeep singh
	*/
	public function test_step2(){
		$output = $this->request("GET", "booking/step2/Mg==");
		
		$this->assertContains("Step2 Code Ok.", $output);
	}
	
	/*
	* version : 1.0.0
	* test_saveStep1()
	* description :  This function is used for testing the saving process of step1.
	* return : text with sucess or false;
	* Author : Pardeep singh
	*/
	public function test_saveStep1(){
		$dummyArray = array( "name" => "John  Doe","Administration" => "test" );
		$output = $this->request("POST", "saveStep1" , $dummyArray);
		$this->assertContains('{"status":"true"}', $output);
	}
}