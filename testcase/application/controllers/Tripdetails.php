	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tripdetails extends MY_Controller {
	
	function __construct() {
        parent::__construct(); 
	}
	/*
	* version : 1.0.0
	* tripDetails()
	* description :  This function is used for listing travel progress.
	* @param : 
	* return template;
	* Author : Pardeep Singh
	*/
	public function tripDetails($param='', $additional= array())
	{
		
		$this->tripProgress($page , $rpp , $offset,$extraArg);	
	}
	
	
	/*
	* version : 1.0.0
	* tripProgress()
	* description :  This function is used to show trips in progress.
	* @param : 
	* return Json;
	* Author : Pardeep Singh
	*/
	public function tripProgress($page='', $rpp="" , $offset="",$post=array()){	
		
		if(ENVIRONMENT == 'testing'){
			echo 'ok tested';
		}else{
			$this->load->view('frontend/xxxxxxx',$this->data);
		}
	}
	
	/*
	* version : 1.0.0
	* sendEmail()
	* description :  This function is used for sending emails.
	* @param : 
	* return template;
	* Updated by : Pardeep Singh
	*/
	public function sendEmail()
	{
		if(ENVIRONMENT == 'testing'){
			echo 'ok tested';
		}else{
			//Working code
		}		
	}
	
}
