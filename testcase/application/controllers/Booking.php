<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends MY_Controller {
	
	function __construct() {
        parent::__construct();
	}
	
	/*
	* version : 1.0.0
	* bookingStep1()
	* description :  This function is used for getting first step1 of booking process.
	* @param : 
	* return template;
	* Author : Pardeep singh
	*/
	public function bookingStep1($param1 = '',$param2=""){
	
		if(ENVIRONMENT == 'testing'){
			echo 'Step1 Code Ok.';					#Testing Case
		}else{
			//Working 
		}
	}
	
	/*
	* version : 1.0.0
	* bookingStep2()
	* description :  This function is used for getting first step2 of booking process.
	* @param : 
	* return template;
	* Author : Pardeep singh
	*/
	public function bookingStep2($param1 = '',$param2="")
	{		
		
		if(ENVIRONMENT == 'testing'){
			echo 'Step2 Code Ok.';					#Testing Case
		}else{
			//Working 
		}
	}
	
	/*
	* version : 1.0.0
	* saveStep1()
	* description :  This function is used for save first step data to cache/session.
	* @param : 
	* return jsonResponse;
	* Author : Pardeep singh
	*/
	public function saveStep1($param = ''){		
		try{
			if(ENVIRONMENT == 'testing'){
				$post	= $this->input->post();
				//Work for post like save data
				$this->sendResponse(json_encode($info),200);
			}	
		}catch(Exception $e) { 
			$this->db->trans_rollback();	 					//Rolled back the commited data			
			$info['status'] 	= 'false';
			$info['message'] 	= $e->getMessage();			
			echo json_encode($info); exit;
		}
		
	}
}
