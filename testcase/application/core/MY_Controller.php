<?php

/*
 * In order to define the main controller 
 * CI allows to create My_Controller 
 * and inherits main CI controller.
 * My_controller created as base controller.
 */

class MY_Controller extends CI_Controller {

    /* Defined constructor for main controller */

    function __construct() {
		
        /* Parent constructor called from CI default controller in child constructor */
        parent::__construct();
		
        global $rootDir, $city;
    }
	
	/*
	* version : 3.0.0
	* sendResponse()
	* description :  Return type, the content header is just information about type of returned data, ex::JSON
	* @param : 
	* return body;
	* Author : Pardeep singh
	*/

	function sendResponse($body = '',$status = 200, $content_type = 'application/json; charset=utf-8') {
		if(ENVIRONMENT != 'testing'){
			$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->getStatusCodeMessage($status);
			header($status_header);
			header('Content-type: ' . $content_type);
			echo $body;exit;
		}else{
			echo $body;
		}
    }
}

?>