<?php

class MY_Model extends CI_Model {

    public $table = '';
    public $primary_key = '';
	public $prefix = 'ms_';
	public $Timeformat = '24';
    function __construct() {
        //load the parent constructor
        parent::__construct();
		$this->Timeformat = $this->session->userdata('Timeformat');
    }

	/**
	* @function selectData()
	* @author: Pardeep
	* @description common function to select data
	* Version: 0.1
	*/
	
	public function selectData( $table, $where='', $type='row', $fields='*', $order_by=array(), $group_by="", $likes=array(), $where_in=array(), $where_not_in=array(),$group = array(),$limit = '' ){
		$query = $this->db->select( $fields );
		
		if(!empty($group)){
			$this->db->group_start();	
			foreach($group as $key => $grp):
				$this->db->or_where($key, $grp);
			endforeach;
			$this->db->group_end();
		}
		
		if( !empty( $where ) ){
			$query = $query->where( $where );
		}
		if( !empty( $where_in ) ){
			foreach( $where_in as $key=>$value ){
				$query = $query->where_in( $key, $value );
			}
		}
		if( !empty( $where_not_in ) ){
			foreach( $where_not_in as $key=>$value ){
				$query = $query->where_not_in( $key, $value );
			}
		}
		if( !empty( $likes ) ){
			foreach( $likes as $key=>$value ){
				$query = $query->like( $key, $value );
			}
		}
		if( !empty( $group_by ) ){
			$query = $query->group_by( $group_by );
		}		
		if( !empty( $order_by ) ){
			$query = $query->order_by( $order_by['field'], $order_by['order_by'] );
		}
		if(!empty($limit)){
			$this->db->limit($limit);
		}
		$query = $query->get( $this->prefix.$table );
		if( $type == 'row' ){
			$table_data = $query->row_array();
		}else if( $type == 'num_rows' ){
			$table_data = $query->num_rows();
		}else{
			$table_data = $query->result_array();
		}		
		return $table_data;
		exit;
	}
	/**
	* @function updateData()
	* @author: Pardeep
	* @description common function to update data
	* Version: 0.1
	*/
	public function updateData( $table, $where, $data,$where_in=array()){
	
		if( !empty( $where ) ){
			$this->db->where( $where );
		}
		if( !empty( $where_in ) ){
			foreach( $where_in as $key=>$value ){
				$this->db->where_in( $key, $value );
			}
		}
		if( $this->db->update( $this->prefix.$table, $data ) ){
			return true;
		}else{
			return false;
		}
		exit;
	}
	/**
	* @function insertData()
	* @author: Pardeep
	* @description common function to insert data
	* Version: 0.1
	*/
	public function insertData( $table, $data ){
		if( $this->db->insert( $this->prefix.$table, $data ) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	/**
	* @function insertBatchData()
	* @author: Pardeep
	* @description common function to insert batch array data
	* Version: 0.1
	*/
	public function insertBatchData( $table, $data ){
		if( $this->db->insert_batch( $this->prefix.$table, $data ) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	
	/**
	* @function deleteData()
	* @author: Pardeep
	* @description common function to delete data
	* Version: 0.1
	*/
	public function deleteData( $table, $where, $where_in="" ){
		if( !empty( $where_in ) ){
			foreach( $where_in as $key=>$value ){
				$this->db->where_in($key, $value);
			}
		}
		if( $this->db->delete( $this->prefix.$table, $where ) ){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	* @function selectWithJoin()
	* @author: Pardeep
	* @description common function to select data with join query.
	* Version: 0.1
	*/
	public function selectWithJoin( $table1,$table2,$where='',$type='result',$fields='*',$joinOn='',$orderBy=array() ){
		$query = $this->db->select( $fields );
		if( !empty( $where ) ){
			$query = $query->where( $where );
		}
		if( !empty($joinOn)){
			$this->db->join($this->prefix.$table2, $joinOn, "left");
		}
		if( !empty( $orderBy ) ){
			$query = $query->order_by( $orderBy['field'], $orderBy['orderBy'] );
			if(isset($orderBy['field2']) && !empty($orderBy['field2'] && !empty($orderBy['orderBy2']))){
				$query = $query->order_by( $orderBy['field2'], $orderBy['orderBy2'] );
			}
		}
		$query = $query->get( $this->prefix.$table1 );
		if( $type == 'row' ){
			$table_data = $query->row_array();
		}else if( $type == 'num_rows' ){
			$table_data = $query->num_rows();
		}else{
			$table_data = $query->result_array();
		}
		return $table_data;
		exit;
	}

}
?>