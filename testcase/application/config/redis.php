<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config = parse_ini_file('/etc/morgan/redis.ini');
/*
| -------------------------------------------------------------------------
| Redis settings
| -------------------------------------------------------------------------
| Your redis servers can be specified below.
|
|	See: https://www.codeigniter.com/userguide3/libraries/caching.html#redis-caching
|
*/
$config['socket_type'] = 'tcp'; //`tcp` or `unix`
// $config['socket'] = '/var/run/redis.sock'; // in case of `unix` socket type
// $config['host'] = '127.0.0.1';
$config['password'] = NULL;
$config['port'] = 6379;
$config['timeout'] = 0;